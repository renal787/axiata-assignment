# axiata-assignment
Backend services for axiata assignment 

### How to run http server on local
```bash
go run cmd/http/main.go
```

### API
## Insert Post
endpoint: localhost:8080/api/posts
method: POST
request body:
{
    "title": "BLOG 16",
    "content": "content",
    "status":"draft",
    "tags":[
        "API", "NEW", "TAG"
    ]
}

## Get Post
endpoint: localhost:8080/api/posts/{post_id}
method: GET

## Get Post By Label
endpoint: localhost:8080/api/posts/?label={label}
method: GET

## Update Post
endpoint: localhost:8080/api/posts/{post_id}
method: PUT
request body:
{
    "title": "BLOG 16 update",
    "content": "content update",
    "tags":[
        "UPDATE"
    ]
}

## Delete Post
endpoint: localhost:8080/api/posts/{post_id}
method: DELETE

