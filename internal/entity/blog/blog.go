package blog

import "time"

type Post struct {
	ID          int       `db:"id" json:"post_id"`
	Title       string    `db:"title" json:"title"`
	Content     string    `db:"content" json:"content"`
	Status      string    `db:"status" json:"status"`
	PublishDate time.Time `db:"publish_date" json:"publish_date"`
	Tags        []string  `json:"tags"`
}

type Tag struct {
	ID    int    `db:"id" json:"tag_id"`
	Label string `db:"label" json:"label"`
}
