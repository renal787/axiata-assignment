package blog

import (
	"context"
	"fmt"
	"postgre-axiata/internal/entity/blog"
	"sync"
)

func (s Service) InsertPost(ctx context.Context, post blog.Post) error {
	var (
		err     error
		wg                 = new(sync.WaitGroup)
		errChan chan error = make(chan error)
		tagIDs  []int
		postID  int
	)

	wg.Add(2)
	go func() {
		defer wg.Done()

		id, err := s.blogData.InsertPost(ctx, post)
		if err != nil {
			errChan <- fmt.Errorf("[SERVICE][InsertPost] %s", err.Error())
		}
		postID = id
	}()

	go func() {
		tagsID, err := s.GetTagsID(ctx, post.Tags)
		if err != nil {
			errChan <- fmt.Errorf("[SERVICE][InsertPost] %s", err.Error())
		}
		tagIDs = append(tagIDs, tagsID...)

		defer wg.Done()
	}()

	go func() {
		wg.Wait()
		defer close(errChan)
	}()

	err = <-errChan
	if err != nil {
		return fmt.Errorf("[SERVICE][InsertPost] %s", err.Error())
	}

	err = s.blogData.InsertPostTag(ctx, int(postID), tagIDs)
	if err != nil {
		return fmt.Errorf("[SERVICE][InsertPost] %s", err.Error())
	}

	return err
}

func (s Service) UpdatePost(ctx context.Context, post blog.Post) error {
	var (
		errChan chan error = make(chan error)
		wg                 = new(sync.WaitGroup)
		tagsID  []int
	)

	wg.Add(3)
	go func() {
		defer wg.Done()

		err := s.blogData.DeletePostTag(ctx, post.ID)
		if err != nil {
			errChan <- err
		}
	}()

	go func() {
		defer wg.Done()

		err := s.blogData.UpdatePost(ctx, post)
		if err != nil {
			errChan <- err
		}
	}()

	go func() {
		defer wg.Done()
		
		result, err := s.GetTagsID(ctx, post.Tags)
		if err != nil {
			errChan <- err
		}

		tagsID = append(tagsID, result...)
	}()

	go func() {
		wg.Wait()
		defer close(errChan)
	}()

	err := <-errChan
	if err != nil {
		return fmt.Errorf("[SERVICE][UpdatePost] %s", err.Error())
	}

	err = s.blogData.InsertPostTag(ctx, post.ID, tagsID)
	if err != nil {
		return fmt.Errorf("[SERVICE][UpdatePost] %s", err.Error())
	}

	return err
}

func (s Service) GetPostByID(ctx context.Context, postID int) (blog.Post, error) {
	var (
		post blog.Post
		err  error
	)

	post, err = s.blogData.GetPostByID(ctx, postID)
	if err != nil {
		return post, fmt.Errorf("[SERVICE][InsertPost] %s", err.Error())
	}

	post.Tags, err = s.blogData.GetTagByPost(ctx, postID)
	if err != nil {
		return post, fmt.Errorf("[SERVICE][InsertPost] %s", err.Error())
	}

	return post, err
}

func (s Service) GetPostByTag(ctx context.Context, tag string) ([]blog.Post, error) {
	var (
		posts []blog.Post
		err   error
	)

	posts, err = s.blogData.GetPostByTag(ctx, tag)
	if err != nil {
		return posts, fmt.Errorf("[SERVICE][InsertPost] %s", err.Error())
	}

	for i, post := range posts {
		tags, err := s.blogData.GetTagByPost(ctx, post.ID)
		if err != nil {
			return posts, fmt.Errorf("[SERVICE][InsertPost] %s", err.Error())
		}

		posts[i].Tags = tags
	}

	return posts, err
}

func (s Service) DeletePost(ctx context.Context, postID int) error {

	err := s.blogData.DeletePost(ctx, postID)
	if err != nil {
		return fmt.Errorf("[SERVICE][DeletePost] %s", err.Error())
	}

	return err
}
