package blog

import (
	"context"
	"postgre-axiata/internal/entity/blog"
)

type BlogData interface {
	InsertPost(ctx context.Context, post blog.Post) (int, error)
	UpdatePost(ctx context.Context, post blog.Post) error
	GetPostByID(ctx context.Context, postID int) (blog.Post, error)
	GetPostByTag(ctx context.Context, tags ...string) ([]blog.Post, error)
	DeletePost(ctx context.Context, postID int) error

	InsertTag(ctx context.Context, label string) (int, error)
	GetTagByLabel(ctx context.Context, label string) (blog.Tag, error)
	GetTagByPost(ctx context.Context, postID int) ([]string, error)

	InsertPostTag(ctx context.Context, postID int, tagIDs []int) error
	DeletePostTag(ctx context.Context, postID int) error
}

type (
	Service struct {
		blogData BlogData
	}
)

func New(blogData BlogData) Service {
	return Service{
		blogData: blogData,
	}
}
