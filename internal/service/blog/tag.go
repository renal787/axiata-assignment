package blog

import (
	"context"
	"database/sql"
	"fmt"
	"postgre-axiata/internal/entity/blog"
	"sync"
)

func (s Service) GetTag(ctx context.Context, label string) (blog.Tag, error) {
	var (
		tag   blog.Tag
		tagID int
		err   error
	)

	tag, err = s.blogData.GetTagByLabel(ctx, label)
	if err != nil {
		if err == sql.ErrNoRows {
			tagID, err = s.blogData.InsertTag(ctx, label)
			if err != nil {
				return tag, fmt.Errorf("[SERVICE][GetTag] %s", err.Error())
			}

			tag = blog.Tag{
				ID:    int(tagID),
				Label: label,
			}
		} else {
			return tag, fmt.Errorf("[SERVICE][GetTag] %s", err.Error())
		}
	}

	return tag, err
}

func (s Service) GetTagsID(ctx context.Context, tags []string) ([]int, error) {
	var (
		wgTag              = new(sync.WaitGroup)
		tagChan chan int   = make(chan int)
		errChan chan error = make(chan error)
		tagsID  []int
	)

	wgTag.Add(len(tags))
	for _, label := range tags {
		go func(label string) {
			defer wgTag.Done()

			tag, err := s.GetTag(ctx, label)
			if err != nil {
				errChan <- fmt.Errorf("[SERVICE][GetTagsID] %s", err.Error())
			}

			tagChan <- int(tag.ID)
		}(label)
	}

	go func() {
		wgTag.Wait()
		defer close(tagChan)
	}()

	go func() {
		for item := range tagChan {
			tagsID = append(tagsID, item)
		}
		defer close(errChan)
	}()

	err := <-errChan
	if err != nil {
		return tagsID, fmt.Errorf("[SERVICE][GetTagsID] %s", err.Error())
	}

	return tagsID, err
}
