package blog

import (
	"fmt"
	"io"
	"net/http"
	"postgre-axiata/internal/entity/blog"
	"postgre-axiata/internal/pkg/response"
	"strconv"

	"github.com/gorilla/mux"
	jsoniter "github.com/json-iterator/go"
)

func (h *Handler) InsertPost(w http.ResponseWriter, r *http.Request) {
	var (
		resp    response.Response
		json    = jsoniter.ConfigCompatibleWithStandardLibrary
		request blog.Post
	)
	defer resp.RenderJSON(w, r)
	ctx := r.Context()

	body, err := io.ReadAll(r.Body)
	if err != nil {
		resp.SetError(err, 400)
		fmt.Printf("[ERROR] %s %s - %v\n", r.Method, r.URL, err)
		return
	}

	err = json.Unmarshal(body, &request)
	if err != nil {
		resp.SetError(err, 400)
		fmt.Printf("[ERROR] %s %s - %v\n", r.Method, r.URL, err)
		return
	}

	err = h.blogService.InsertPost(ctx, request)
	if err != nil {
		resp.SetError(err, 400)
		fmt.Printf("[ERROR] %s %s - %v\n", r.Method, r.URL, err)
		return
	}

	fmt.Printf("[INFO] %s %s\n", r.Method, r.URL)
}

func (h *Handler) UpdatePost(w http.ResponseWriter, r *http.Request) {
	var (
		resp    response.Response
		json    = jsoniter.ConfigCompatibleWithStandardLibrary
		request blog.Post
	)
	defer resp.RenderJSON(w, r)
	ctx := r.Context()
	vars := mux.Vars(r)
	postID, err := strconv.Atoi(vars["post_id"])
	if err != nil {
		resp.SetError(err, 400)
		fmt.Printf("[ERROR] %s %s - %v\n", r.Method, r.URL, err)
		return
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		resp.SetError(err, 400)
		fmt.Printf("[ERROR] %s %s - %v\n", r.Method, r.URL, err)
		return
	}

	err = json.Unmarshal(body, &request)
	if err != nil {
		resp.SetError(err, 400)
		fmt.Printf("[ERROR] %s %s - %v\n", r.Method, r.URL, err)
		return
	}

	request.ID = postID
	err = h.blogService.UpdatePost(ctx, request)
	if err != nil {
		resp.SetError(err, 400)
		fmt.Printf("[ERROR] %s %s - %v\n", r.Method, r.URL, err)
		return
	}

	fmt.Printf("[INFO] %s %s\n", r.Method, r.URL)
}

func (h *Handler) GetPostByID(w http.ResponseWriter, r *http.Request) {
	var (
		resp   response.Response
		result interface{}
	)
	defer resp.RenderJSON(w, r)
	ctx := r.Context()
	vars := mux.Vars(r)
	postID, err := strconv.Atoi(vars["post_id"])
	if err != nil {
		resp.SetError(err, 400)
		fmt.Printf("[ERROR] %s %s - %v\n", r.Method, r.URL, err)
		return
	}

	result, err = h.blogService.GetPostByID(ctx, postID)
	if err != nil {
		resp.SetError(err, 400)
		fmt.Printf("[ERROR] %s %s - %v\n", r.Method, r.URL, err)
		return
	}

	resp.Data = result
	fmt.Printf("[INFO] %s %s\n", r.Method, r.URL)
}

func (h *Handler) GetPostByTag(w http.ResponseWriter, r *http.Request) {
	var (
		resp   response.Response
		result interface{}
		err    error
	)
	defer resp.RenderJSON(w, r)
	ctx := r.Context()

	result, err = h.blogService.GetPostByTag(ctx, r.FormValue("label"))
	if err != nil {
		resp.SetError(err, 400)
		fmt.Printf("[ERROR] %s %s - %v\n", r.Method, r.URL, err)
		return
	}

	resp.Data = result
	fmt.Printf("[INFO] %s %s\n", r.Method, r.URL)
}

func (h *Handler) DeletePost(w http.ResponseWriter, r *http.Request) {
	var (
		resp response.Response
	)
	defer resp.RenderJSON(w, r)
	ctx := r.Context()
	vars := mux.Vars(r)
	postID, err := strconv.Atoi(vars["post_id"])
	if err != nil {
		resp.SetError(err, 400)
		fmt.Printf("[ERROR] %s %s - %v\n", r.Method, r.URL, err)
		return
	}

	err = h.blogService.DeletePost(ctx, postID)
	if err != nil {
		resp.SetError(err, 400)
		fmt.Printf("[ERROR] %s %s - %v\n", r.Method, r.URL, err)
		return
	}

	fmt.Printf("[INFO] %s %s\n", r.Method, r.URL)
}
