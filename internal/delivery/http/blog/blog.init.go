package blog

import (
	"context"
	"postgre-axiata/internal/entity/blog"
)

type BlogService interface {
	InsertPost(ctx context.Context, post blog.Post) error
	UpdatePost(ctx context.Context, post blog.Post) error
	GetPostByID(ctx context.Context, postID int) (blog.Post, error)
	GetPostByTag(ctx context.Context, tag string) ([]blog.Post, error)
	DeletePost(ctx context.Context, postID int) error
}

type (
	Handler struct {
		blogService BlogService
	}
)

func New(blogService BlogService) *Handler {
	return &Handler{
		blogService: blogService,
	}
}
