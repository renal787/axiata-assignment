package http

import (
	"errors"
	"net/http"
	"postgre-axiata/internal/pkg/response"

	"github.com/gorilla/mux"
)

func (s *Server) Handler() *mux.Router {
	r := mux.NewRouter()

	// Jika tidak ditemukan.
	r.NotFoundHandler = http.HandlerFunc(notFoundHandler)

	// Tambahan Prefix di depan API endpoint
	api := r.PathPrefix("/api").Subrouter()

	postBlog := api.PathPrefix("/posts").Subrouter()
	postBlog.HandleFunc("", s.BlogHandler.InsertPost).Methods("POST")
	postBlog.HandleFunc("/", s.BlogHandler.GetPostByTag).Queries("label", "{label}").Methods("GET")
	postBlog.HandleFunc("/{post_id}", s.BlogHandler.GetPostByID).Methods("GET")
	postBlog.HandleFunc("/{post_id}", s.BlogHandler.UpdatePost).Methods("PUT")
	postBlog.HandleFunc("/{post_id}", s.BlogHandler.DeletePost).Methods("DELETE")

	return r
}

func notFoundHandler(w http.ResponseWriter, r *http.Request) {
	var (
		resp   *response.Response
		err    error
		errRes response.Error
	)
	resp = &response.Response{}
	defer resp.RenderJSON(w, r)

	err = errors.New("404 Not Found")

	if err != nil {
		// Error response handling
		errRes = response.Error{
			Code:   404,
			Msg:    "404 Not Found",
			Status: true,
		}

		resp.StatusCode = 404
		resp.Error = errRes
		return
	}
}
