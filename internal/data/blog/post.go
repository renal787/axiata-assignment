package blog

import (
	"context"
	"fmt"
	"postgre-axiata/internal/entity/blog"

	"github.com/lib/pq"
)

func (d Data) InsertPost(ctx context.Context, post blog.Post) (int, error) {
	var (
		err    error
		postID int
	)

	err = (*d.stmt)[insertPost].QueryRowContext(ctx, post.Title, post.Content, post.Status).Scan(&postID)
	if err != nil {
		return postID, fmt.Errorf("[DATA][InsertPost] %s", err.Error())
	}

	return postID, err
}

func (d Data) UpdatePost(ctx context.Context, post blog.Post) error {
	var (
		err error
	)

	_, err = (*d.stmt)[updatePost].ExecContext(ctx, post.Title, post.Content, post.ID)
	if err != nil {
		return fmt.Errorf("[DATA][UpdatePost] %s", err.Error())
	}

	return err
}

func (d Data) GetPostByID(ctx context.Context, postID int) (blog.Post, error) {
	var (
		post blog.Post
		err  error
	)

	err = (*d.stmt)[getPostByID].QueryRowContext(ctx, postID).Scan(&post.ID, &post.Title, &post.Content, &post.Status, &post.PublishDate)
	if err != nil {
		return post, fmt.Errorf("[DATA][GetPostByID] %s", err.Error())
	}

	return post, err
}

func (d Data) GetPostByTag(ctx context.Context, tags ...string) ([]blog.Post, error) {
	var (
		posts []blog.Post
		err   error
	)

	rows, err := (*d.stmt)[getPostByTag].QueryContext(ctx, pq.Array(tags))
	if err != nil {
		return posts, fmt.Errorf("[DATA][GetPostByTag] %s", err.Error())
	}
	defer rows.Close()

	for rows.Next() {
		var (
			post blog.Post
		)

		if err := rows.Scan(&post.ID, &post.Title, &post.Content, &post.Status, &post.PublishDate); err != nil {
			return posts, fmt.Errorf("[DATA][GetPostByTag] %s", err.Error())
		}

		posts = append(posts, post)
	}

	return posts, err
}

func (d Data) DeletePost(ctx context.Context, postID int) error {
	var (
		err error
	)

	_, err = (*d.stmt)[deletePost].ExecContext(ctx, postID)
	if err != nil {
		return fmt.Errorf("[DATA][DeletePost] %s", err.Error())
	}

	return err
}
