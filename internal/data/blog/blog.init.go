package blog

import (
	"context"
	"database/sql"
	"log"
)

type (
	Data struct {
		db   *sql.DB
		stmt *map[string]*sql.Stmt
	}

	statement struct {
		key   string
		query string
	}
)

const (
	// Blog post Table
	insertPost  = "InsertPost"
	qInsertPost = `INSERT INTO public.post (title, content, status, publish_date) VALUES ($1, $2, $3, NOW()) RETURNING id;`

	updatePost  = "UpdatePost"
	qUpdatePost = `UPDATE public.post SET title = $1, content = $2 WHERE id = $3;`

	getPostByID  = "GetPostByID"
	qGetPostByID = `SELECT id, title, content, status, publish_date FROM public.post WHERE id = $1;`

	getPostByTag  = "GetPostByTag"
	qGetPostByTag = `SELECT p.id, p.title, p.content, p.status, p.publish_date 
	FROM public.post p JOIN (
		SELECT pt.post_id 
		FROM public.post_tag pt 
		JOIN tag t 
		ON pt.tag_id = t.id 
		WHERE t.label = ANY($1::varchar[])
		) AS tag 
	ON tag.post_id = p.id;`

	deletePost  = "DeletePost"
	qDeletePost = `DELETE FROM public.post WHERE id = $1;`

	// Blog tag Table
	insertTag  = "InsertTag"
	qInsertTag = `INSERT INTO public.tag (label) VALUES ($1) RETURNING id;`

	getTagByLabel  = "GetTagByLabel"
	qGetTagByLabel = `SELECT id, label FROM public.tag WHERE label = $1;`

	getTagByPost  = "GetTagByPost"
	qGetTagByPost = `SELECT t.label FROM public.tag t JOIN public.post_tag pt ON t.id = pt.tag_id WHERE pt.post_id = $1;`

	// Blog post_tag Table
	insertPostTag  = "InsertPostTag"
	qInsertPostTag = `INSERT INTO public.post_tag (post_id, tag_id) VALUES ($1, $2);`

	deletePostTag = "DeletePostTag"
	qDeletePostTag = `DELETE FROM public.post_tag WHERE post_id = $1`
)

var (
	readStmt = []statement{
		{getPostByID, qGetPostByID},
		{getPostByTag, qGetPostByTag},
		{getTagByLabel, qGetTagByLabel},
		{getTagByPost, qGetTagByPost},
	}
	insertStmt = []statement{
		{insertPost, qInsertPost},
		{insertTag, qInsertTag},
		{insertPostTag, qInsertPostTag},
	}
	updateStmt = []statement{
		{updatePost, qUpdatePost},
	}
	deleteStmt = []statement{
		{deletePost, qDeletePost},
		{deletePostTag, qDeletePostTag},
	}
)

// New ...
func New(db *sql.DB) *Data {
	var (
		stmts = make(map[string]*sql.Stmt)
	)

	d := &Data{
		db:   db,
		stmt: &stmts,
	}

	d.InitStmt()

	return d
}

func (d *Data) InitStmt() {
	var (
		err   error
		stmts = make(map[string]*sql.Stmt)
	)

	for _, v := range readStmt {
		stmts[v.key], err = d.db.PrepareContext(context.Background(), v.query)
		if err != nil {
			log.Fatalf("[DB] Failed to initialize select statement key %v, err : %v", v.key, err)
		}
	}

	for _, v := range insertStmt {
		stmts[v.key], err = d.db.PrepareContext(context.Background(), v.query)
		if err != nil {
			log.Fatalf("[DB] Failed to initialize insert statement key %v, err : %v", v.key, err)
		}
	}

	for _, v := range updateStmt {
		stmts[v.key], err = d.db.PrepareContext(context.Background(), v.query)
		if err != nil {
			log.Fatalf("[DB] Failed to initialize update statement key %v, err : %v", v.key, err)
		}
	}

	for _, v := range deleteStmt {
		stmts[v.key], err = d.db.PrepareContext(context.Background(), v.query)
		if err != nil {
			log.Fatalf("[DB] Failed to initialize delete statement key %v, err : %v", v.key, err)
		}
	}

	*d.stmt = stmts
}
