package blog

import (
	"context"
	"fmt"
	"postgre-axiata/internal/entity/blog"
)

func (d Data) InsertTag(ctx context.Context, label string) (int, error) {
	var (
		err   error
		tagID int
	)

	err = (*d.stmt)[insertTag].QueryRowContext(ctx, label).Scan(&tagID)
	if err != nil {
		return tagID, fmt.Errorf("[DATA][InsertTag] %s", err.Error())
	}

	return tagID, err
}

func (d Data) GetTagByLabel(ctx context.Context, label string) (blog.Tag, error) {
	var (
		err error
		tag blog.Tag
	)

	err = (*d.stmt)[getTagByLabel].QueryRowContext(ctx, label).Scan(&tag.ID, &tag.Label)
	if err != nil {
		return tag, err
	}

	return tag, err
}

func (d Data) GetTagByPost(ctx context.Context, postID int) ([]string, error) {
	var (
		err error
		tags []string
	)

	rows, err := (*d.stmt)[getTagByPost].QueryContext(ctx, postID)
	if err != nil {
		return tags, err
	}
	defer rows.Close()

	for rows.Next() {
		var tag string

		if err := rows.Scan(&tag); err != nil {
			return tags, err
		}

		tags = append(tags, tag)
	}

	return tags, err
}
