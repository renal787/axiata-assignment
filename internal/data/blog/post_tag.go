package blog

import (
	"context"
	"fmt"
	"sync"
)

func (d Data) InsertPostTag(ctx context.Context, postID int, tagIDs []int) error {
	var (
		err error
		errChan chan error = make(chan error)
		wg                 = new(sync.WaitGroup)
	)


	for i := 0; i < len(tagIDs); i += 5 {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			chunk := tagIDs[i:min(i+5, len(tagIDs))]
			for _, tagID := range chunk {
				_, err := (*d.stmt)[insertPostTag].ExecContext(ctx, postID, tagID)
				if err != nil {
					errChan <- err
				}
			}
		}(i)
	}

	go func ()  {
		wg.Wait()
		defer close(errChan)
	}()

	err = <- errChan
	if err != nil {
		return fmt.Errorf("[DATA][InsertPostTag] %s", err.Error())
	}

	return err
}

func (d Data) DeletePostTag(ctx context.Context, postID int) error {
	var (
		err error
	)

	_, err = (*d.stmt)[deletePostTag].ExecContext(ctx, postID)
	if err != nil {
		return fmt.Errorf("[DATA][DeletePostTag] %s", err.Error())
	}

	return err
}
