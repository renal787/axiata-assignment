package response

import (
	"encoding/json"
	"net/http"
)

// Response defines http response for the client
type Response struct {
	Data       interface{} `json:"data,omitempty"`
	Metadata   interface{} `json:"metadata,omitempty"`
	Error      Error       `json:"error"`
	StatusCode int         `json:"-"`
}

// Error defines the error
type Error struct {
	Status bool   `json:"status"` // true if we have error
	Msg    string `json:"msg"`    // error message
	Code   int    `json:"code"`   // error code from application, it is not http status code
}

// RenderJSON writes the http response in JSON format to the client
func (res *Response) RenderJSON(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	if res.StatusCode == 0 {
		res.StatusCode = http.StatusOK
	}

	d, err := json.Marshal(res)
	if err != nil {
		res.StatusCode = http.StatusInternalServerError
	}

	w.WriteHeader(res.StatusCode)
	w.Write(d)
}

func (res *Response) SetError(err error, code int) {
	if code == 0 {
		code = 200
	}

	if err != nil {
		res.Error = Error{
			Code:   code,
			Msg:    err.Error(),
			Status: true,
		}
	}

}
