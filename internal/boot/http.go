package boot

import (
	"database/sql"
	"log"
	"net/http"

	blogData "postgre-axiata/internal/data/blog"
	server "postgre-axiata/internal/delivery/http"
	blogHandler "postgre-axiata/internal/delivery/http/blog"
	blogService "postgre-axiata/internal/service/blog"
)

func HTTP() error {
	var (
		err error
	)

	db, err := openConnectionPool("postgres", "user=postgres dbname=postgres sslmode=disable password=myaxiata host=34.101.197.134")
	if err != nil {
		log.Fatalf("[DB] Failed to initialize database connection: %v", err)
	}

	data := blogData.New(db)
	service := blogService.New(data)
	handler := blogHandler.New(service)

	httpServer := server.Server{
		BlogHandler: handler,
	}

	if err := httpServer.Serve(":8080"); err != http.ErrServerClosed {
		return err
	}

	return err
}

// create new connection pool and test the connection
func openConnectionPool(driver string, connString string) (db *sql.DB, err error) {
	if db, err = sql.Open(
		driver,
		connString,
	); err != nil {
		return db, err
	}

	err = db.Ping()
	if err != nil {
		return db, err
	}

	return db, err
}
