package main

import (
	"log"
	"postgre-axiata/internal/boot"
	_ "github.com/lib/pq"
)

func main() {
	if err := boot.HTTP(); err != nil {
		log.Println("[HTTP] failed to boot http server due to " + err.Error())
	}
}
