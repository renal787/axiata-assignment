module postgre-axiata

go 1.22.3

require (
	github.com/gorilla/mux v1.8.1
	github.com/json-iterator/go v1.1.12
	github.com/lib/pq v1.10.9
	github.com/rs/cors v1.11.0
)

require (
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/stretchr/testify v1.8.1 // indirect
)
